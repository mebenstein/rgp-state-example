class UserMode implements DriveMode{

    private int getPedalPosition();

    public void drive(DriveController controller){
        super.drive(controller);

        controller.setSpeed(getPedalPosition()*160);
    }

}
