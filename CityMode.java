class CityMode extends ObstacleAvoidanceMode{
    private List<Pedestrian> getPadestriansInProxy();
    private double getDangerFactor();


    public void drive(DriveController controller){
        List<Pedestrian> pedestrianList = getPadestriansInProxy();

        if(pedestrianList.length > 10) {
            controller.setSpeed(20);
            controller.setMode(new UserMode());
            return;
        }

        controller.setSpeed(40);

        int minDistance = 10;
        for(Pedestrian pedestrian:pedestrianList){
            int pedestrianDistance = pedestrian.predictedPath().distanceInTime(controller.getPosition());

            if(pedestrianDistance < minDistance) {
                minDistance = pedestrianDistance;
                controller.setSpeed(pedestrianDistance);
            }
        }
    }
}
