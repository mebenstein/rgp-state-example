class ObstacleAvoidanceMode implements DriveMode{

    private List<Obstacle> getObstacles();
    private Path adjustPath(List<Obstacle> obstacles);

    public void drive(DriveController controller){
        Path pathAdjustment = adjustPath(getObstacles());

        Path planned = controller.getPlannedPath();

        controller.setPlannedPath(planned.combine(pathAdjustment));
     }

}
