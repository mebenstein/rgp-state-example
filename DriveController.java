 
class DriveController{
    public void setSpeed(double speed);
    public void setMode(DriveMode mode);
    public void setPlannedPath(Path path);
    public Path getPlannedPath(Path path);

    private boolean isCity();

    private DriveMode mode = new UserMode();

    public void controlLoop(){
        mode.drive(this);
    }

    public void selectMode(){
        if(isCity())
            setMode(new CityMode());
        else
            setMode(new HighwayMode());
    }

    public void userTakeover(){
        setMode(new UserMode());
    }
}
