class WinterMode extends ObstacleAvoidanceMode{

    private double getIceRisk();
    private double getSightRisk();

    double riskHistoryScore = 0;

    public void drive(DriveController controller){
        super.drive(controller);

        double riskScore = 0.6 * getIceRisk() + 0.4 * getSightRisk();

        riskHistoryScore *= 0.9;
        riskHistoryScore += riskScore;

        if(riskHistoryScore > 90) {
            controller.setSpeed(30);
            controller.setMode(new UserMode());
        }else if(riskHistoryScore > 75){
            controller.setSpeed(40);
        }else if(riskHistoryScore < 20)
            controller.setMode(new HighwayMode());
        else
            controller.setSpeed(120-riskScore);
     }

}
